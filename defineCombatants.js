// CREATE COMBATANT AND DEFINE ATTRIBUTES

function Combatant(setName, setHealth, setPower, setEvasion, setResilience, setDeck){
	this.name = setName;
	this.health = setHealth;
	this.power = setPower;
	this.evasion = setEvasion;
	this.resilience = setResilience;
  this.deck = setDeck;
  this.hand = [];
  this.deckRemaining = setDeck;
  this.alive = true;
}

var deck_scruffy = [
  "BackOff",
  "SortaStronger",
  "PowerSmash",
  "ALittleSlow",
  "TheQuickener",
  "PowderPuff",
  "BackOff",
  "SortaStronger",
  "PowerSmash",
  "ALittleSlow",
  "TheQuickener",
  "PowderPuff",
  "BackOff",
  "SortaStronger",
  "PowerSmash",
  "ALittleSlow",
  "TheQuickener",
  "PowderPuff",
  "BackOff",
  "SortaStronger",
  "PowerSmash",
  "ALittleSlow",
  "TheQuickener",
  "PowderPuff"
];

var scruffy = new Combatant(
	//name
	"ScruffyDux",
	//health
	500,
	//power
	100,
	//evasion
	40,
	//resilience
	60,
  //deck
  deck_scruffy
);

var deck_uglyface = [
  "BackOff",
  "SortaStronger",
  "PowerSmash",
  "ALittleSlow",
  "TheQuickener",
  "PowderPuff",
  "BackOff",
  "SortaStronger",
  "PowerSmash",
  "ALittleSlow",
  "TheQuickener",
  "PowderPuff",
  "BackOff",
  "SortaStronger",
  "PowerSmash",
  "ALittleSlow",
  "TheQuickener",
  "PowderPuff",
  "BackOff",
  "SortaStronger",
  "PowerSmash",
  "ALittleSlow",
  "TheQuickener",
  "PowderPuff"
];

var uglyface = new Combatant(
	//name
	"Ugly Face Scary Pants",
	//health
	500,
	//power
	300,
	//evasion
	15,
	//resilience
	25,
  //deck
  deck_uglyface
);

var player = scruffy;
var enemy = uglyface;
