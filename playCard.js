// PLAY CARD

function playCard( setSelf, setOpponent, setCard ){

	this.self = setSelf;
	this.opponent = setOpponent;
	this.card = setCard;

  //find full card details
	var playedCard = AllCards[card];

	var target = playedCard.target;
	var appliesTo = playedCard.appliesTo;
	var modifyBy = playedCard.modifyBy;

	// log whose playing and the card they chose

	updateBattleLog.update(
		self.name + " plays the '" + playedCard.cardName + "' card"
	);

	// log what's about to be changed

	updateBattleLog.update(
		this[target]["name"] + "'s current " + appliesTo + " is " +  this[target][appliesTo]
	);

	// log what's going to happen

	updateBattleLog.update(
		this[target]["name"] + "'s " + appliesTo + " " + playedCard.cardAttribute + "s by " +  playedCard.cardValue + " " +  playedCard.modifyBy
	);


	// check if the card's target is opponent or self
	switch (playedCard.target) {

		// If attacking opponent reduce the attribute
		case "opponent":

			if (playedCard.modifyBy == "percent"){
				var amountToAlter = this[target][appliesTo] * (playedCard.cardValue / 100);
			} else {
				var amountToAlter = playedCard.cardValue;
			}

			this[target][appliesTo] -= amountToAlter;

			break;

		// If buffing self increase the attribute
		case "self":

			if (playedCard.modifyBy == "percent"){
				var amountToAlter = this[target][appliesTo] * (playedCard.cardValue / 100);
			} else {
				var amountToAlter = playedCard.cardValue;
			}

			this[target][appliesTo] += amountToAlter;

			break;
	}

	// log what's just happened

	updateBattleLog.update(
		this[target]["name"] + "'s " + appliesTo + " is now " + this[target][appliesTo]
	);

	showPlayerStats.update();
	showEnemyStats.update();

}
