function pickRandomProperty(obj) {
    var result;
    var count = 0;
    for (var prop in obj)
        if (Math.random() < 1/++count)
           result = prop;
    return result;
}

function pickRandomThenRemove(arr){
  var result;
  //generate random number, less than the number of cards
  var randomNum = Math.floor(Math.random() * (arr.length - 1)) + 0;
  var selectFromArray = arr[randomNum];
  arr.splice(randomNum, 1);
  return selectFromArray;
}

function randomFirstTurn(){
	var randomNum = Math.floor(Math.random() * 2) + 1;
	if (randomNum == 1){
		return player;
	} else if (randomNum == 2){
		return enemy;
	}
}
