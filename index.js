
var cardsPerHand = 3;

var currentFirstTurn = randomFirstTurn();
// var currentFirstTurn = enemy;

var roundNumber = 1;

// console.log(player.deck);

var cardDragged = "";
var cardHTML = "";


function dealFirstHand(cardCount){

	for (i = 0; i < cardCount; i++) {

		player.hand.push( pickRandomThenRemove(player.deckRemaining) );
		enemy.hand.push( pickRandomThenRemove(enemy.deckRemaining) );

	}

}

dealFirstHand(cardsPerHand);

function startBattle(){
	playRound();
}

function playRound(){

	if ( player.alive && enemy.alive ){

		appendMessage("<h1>Round Number " + roundNumber + "</h1>");

		// startRound(currentFirstTurn);
		playTurns(currentFirstTurn);

		roundNumber++;

	}
}

function showPlayerHand(){

	var handHTML = "";

	_.each( player.hand, function(card, index){
		var thisCard = AllCards[card];
		var target = thisCard.target == "self" ? player.name : enemy.name;
		// console.log(target);
		handHTML += "<p><input class='playerHand' type='radio' name='playerHand' value='" + card + "' id='" + thisCard.cardID + "'><label>" + thisCard.cardName + "</label></p>" + target + "'s " + thisCard.appliesTo + " " + thisCard.cardAttribute + "s by " +  thisCard.cardValue + " " +  thisCard.modifyBy;
	});

	jQuery(".playerHand").html( handHTML );

}

function appendMessage(msg){
	jQuery(".messages").append("<p>" + msg + "</p>");
}

var showEnemyHealth = new Vue({
	el: '#enemyHealth',
  data: {
		intialHealth: enemy.health,
    health: enemy.health,
		healthPercentage: 100
  },
  created: function () {

  },
	methods: {
		update: function(){
			this.health = enemy.health;
			this.health = this.health <= 0 ? 0 : this.health;
			this.healthPercentage = Math.floor( (this.health / this.intialHealth) * 100 );
		}
	}
})

var showEnemyStats = new Vue({
	el: '#enemyStats',
  data: {
		power: enemy.power,
		evasion: enemy.evasion,
		resilience: enemy.resilience
  },
	methods: {
		update: function(){
			this.power = enemy.power,
			this.evasion = enemy.evasion,
			this.resilience = enemy.resilience
		}
	}
})

var showPlayerHealth = new Vue({
	el: '#playerHealth',
  data: {
		intialHealth: player.health,
    health: player.health,
		healthPercentage: 100
  },
	methods: {
		update: function(){
			this.health = player.health
			this.health = this.health <= 0 ? 0 : this.health;
			this.healthPercentage = Math.floor( (this.health / this.intialHealth) * 100 );
		}
	}
})

var showPlayerStats = new Vue({
	el: '#playerStats',
  data: {
		power: player.power,
		evasion: player.evasion,
		resilience: player.resilience
  },
	methods: {
		update: function(){
			this.power = player.power,
			this.evasion = player.evasion,
			this.resilience = player.resilience
		}
	}
})

Vue.component('card-faceup', {
  props: ['card'],
  template: '<div class="cardFaceUp" v-bind="{ id: card.id }" draggable="true" v-on:dragstart="cardDragged(card.cardID, card.id)" v-on:click="cardSelected(card.cardID)"><div class="cardName">{{ card.cardName }}</div><div class="cardDescription">{{card.target}}\'s {{card.appliesTo}} {{card.cardAttribute}}s by {{card.cardValue}} {{card.modifyBy}}</div></div>',
	methods: {
		cardSelected: function(val){
			// console.log(val);
			// playerPlayCard( val );
		},
		cardDragged: function(card, id){
			cardDragged = card;
			console.log(cardDragged);
			cardHTML = $( "#" + id ).html();
			console.log(cardHTML);
		}
	}
})

var playerCards = new Vue({
  el: '#playerCards',
  data: {
    playerHand: [
      { id: "card1", cardName: "", cardID: "", appliesTo: "", cardAttribute: "", cardValue: "", modifyBy: "", target: "" },
      { id: "card2", cardName: "", cardID: "", appliesTo: "", cardAttribute: "", cardValue: "", modifyBy: "", target: "" },
      { id: "card3", cardName: "", cardID: "", appliesTo: "", cardAttribute: "", cardValue: "", modifyBy: "", target: "" }
    ]
  },
	methods: {
		update: function(card){
			_.each(this.playerHand, function(card, index){
				var thisCard = AllCards[player.hand[index]];
				var target = thisCard.target == "self" ? player.name : enemy.name;

				card.target = target;
				card.cardName = thisCard.cardName;
				card.cardID = thisCard.cardID;
				card.appliesTo = thisCard.appliesTo;
				card.cardAttribute = thisCard.cardAttribute;
				card.cardValue = thisCard.cardValue;
				card.modifyBy = thisCard.modifyBy;
			});
		}
	}
})

playerCards.update();

function updateStats(){
	showPlayerStats.update();
	showEnemyStats.update();
}

function updateHealth(){
	showPlayerHealth.update();
	showEnemyHealth.update();
}

var updateBattleLog = new Vue({
	el: '#battleLog',
  data: {
		logContent: ""
  },
	methods: {
		update: function(newContent){
			// this.logContent += "<p>" + newContent + "</p>"
			console.log(newContent);
		}
	}
})

var cardDropped = new Vue({
  el: '#playerPlayArea',
	data: {
		playedCard: ""
	},
  methods: {
    drop: function(e) {
      console.log('Looks like you dropped something!', cardDragged);
			playerPlayCard( cardDragged );
			this.playedCard = "<div class='cardFaceUp'>" + cardHTML + "</div>";
    }
  }
})
