// DEFINE POSSIBLE CARD ATTRIBUTES

var cardAttributes = {
	"weaken": { "appliesTo": "power", "target": "opponent", "modifyBy": "flat" },
	"slow": { "appliesTo": "evasion", "target": "opponent", "modifyBy": "percent" },
	"soften": { "appliesTo": "resilience", "target": "opponent", "modifyBy": "percent" },
	"strengthen": { "appliesTo": "power", "target": "self", "modifyBy": "flat" },
	"accelerate": { "appliesTo": "evasion", "target": "self", "modifyBy": "percent" },
	"boost": { "appliesTo": "resilience", "target": "self", "modifyBy": "percent" }
};

var AllCards = {};

function Card( setCardName, setCardAttribute, setCardValue ){
  this.cardID = setCardName;
	var beautifyName = setCardName.replace(/([A-Z])/g, ' $1').trim()
	this.cardName = beautifyName;
	this.cardAttribute = setCardAttribute;
	this.cardValue = setCardValue;
	this.target = cardAttributes[this.cardAttribute].target;
	this.appliesTo = cardAttributes[this.cardAttribute].appliesTo;
	this.modifyBy = cardAttributes[this.cardAttribute].modifyBy;

	AllCards[setCardName] = this;
}

// DEFINE CARDS

// Card => cardAttribute, cardValue
new Card( "BackOff", "weaken", 30);

new Card( "SortaStronger", "strengthen", 30 );

new Card( "PowerSmash", "strengthen", 100 );

new Card( "ALittleSlow", "slow", 5 );

new Card( "TheQuickener", "accelerate", 50 );

new Card( "PowderPuff", "soften", 50 );


/*_.each( AllCards, function(cardProperties, cardName){
	console.log(cardName);
	console.log(cardProperties);
});*/
