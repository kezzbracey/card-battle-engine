var playerFirst = true;

var waitingForPlayerSelection = false;

function playTurns( firstTurn ){

	if (firstTurn == player) {

    updateBattleLog.update("Your turn, " + player.name);

    playerFirst = true;

    playerTurn();

	} else {

    playerFirst = false;

    enemyTurn();

	}

}

function playerTurn() {
  waitingForPlayerSelection = true;
  updateBattleLog.update("player choosing...");
}

function playerPlayCard(playerCard){

	console.log("play");

  if (waitingForPlayerSelection) {

    waitingForPlayerSelection = false;

    playCard( player, enemy, playerCard );

    //remove played card from hand
    var indexOfCard = _.indexOf(player.hand, playerCard);
    player.hand.splice(indexOfCard, 1);

    //player draw new card
    player.hand.push( pickRandomThenRemove(player.deckRemaining) );

		showPlayerHand();

    if (playerFirst){
      updateBattleLog.update("----------------------");
      enemyTurn();
    } else {
      updateBattleLog.update("----------------------");
      evaluateTurns(currentFirstTurn);
    }

  }

}

function enemyTurn(){
  updateBattleLog.update("enemy choosing...");
  //enemy plays card
  var enemyCard = "BackOff";
  var enemyCard = pickRandomThenRemove(enemy.hand);
  playCard( enemy, player, enemyCard );

  //enemy draw new card
  enemy.hand.push( pickRandomThenRemove(enemy.deckRemaining) );

  if (!playerFirst){
    updateBattleLog.update("----------------------");
    playerTurn();
  } else {
    updateBattleLog.update("----------------------");
    evaluateTurns(currentFirstTurn);
  }
}

function evaluateTurns( firstTurn ){

  updateBattleLog.update("<h2>Calculating Damage</h2>");

	if (firstTurn == player) {

		updateBattleLog.update(player.name + " attacks first");
		// attacker, defender
		calculateDamage(player, enemy);

		if (enemy.health <= 0) {
			updateBattleLog.update( "<h2>Enemy has been defeated!!</h2>" );
			enemy.alive = false;

		//only evaluate second turn if first combatant not dead
		} else {

			updateBattleLog.update("----------------------");

			updateBattleLog.update(enemy.name + " attacks second");
			calculateDamage(enemy, player);
			if (player.health <= 0) {
				updateBattleLog.update( "<h2>Oh no, you died!!</h2>" );
				player.alive = false;
			}
		}

	} else {

		updateBattleLog.update(enemy.name + " attacks first");
		// attacker, defender
		calculateDamage(enemy, player);

		if (player.health <= 0) {
			updateBattleLog.update( "<h2>Oh no, you died!!</h2>" );
			player.alive = false;

		//only evaluate second turn if first combatant not dead
		} else {

			updateBattleLog.update("----------------------");

			updateBattleLog.update(player.name + " attacks second");
			calculateDamage(player, enemy);
			if (enemy.health <= 0) {
				updateBattleLog.update( "<h2>Enemy has been defeated!!</h2>" );
				enemy.alive = false;
			}

		}

	}

  //switch first turn for next round
  currentFirstTurn = currentFirstTurn == player ? enemy : player;

  //play the next round round
  playRound();

}


function calculateDamage(attacker, defender){
	updateBattleLog.update(defender.name + "'s health is " + defender.health);

	updateBattleLog.update(attacker.name + " attacks with " + attacker.power + " power");

	var evasionAmount = Math.floor( attacker.power * (defender.evasion / 100) );

	updateBattleLog.update(defender.name + " evades " + evasionAmount + " of the attack");

	var attackPower = Math.floor( attacker.power - evasionAmount );

	var resilienceAmount = Math.floor( attackPower * (defender.resilience / 100) );

	updateBattleLog.update(defender.name + " is resilient to another " + resilienceAmount + " of the attack");

	attackPower = Math.floor( attackPower - resilienceAmount );

	updateBattleLog.update(defender.name + " takes " + attackPower + " damage");

	defender.health -= attackPower;

	updateBattleLog.update(defender.name + "'s' health is now " + defender.health );

	updateHealth();
	updateStats();

}
